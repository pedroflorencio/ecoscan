import os
import numpy as np
import pandas as pd
from shutil import move
from random import seed

def train_test_separator(labels:str):
    # constantes
    seed(42)
    PATH_IMAGES = labels
    TYPES_GARBAGE = ['glass','cardboard','plastic','paper','trash','metal']

    # acessando o arquivo txt como DataFrame
    df = pd.read_csv(PATH_IMAGES, 
                    sep=' ',
                    header=None)

    # renomeando colunas do DataFrame
    df.rename(columns={0:'image',1:'label'}, inplace=True)

    # selecionando aleatoriamente as imagens de teste para cada tipo de residuo, de forma proporcional
    for type_garbage in TYPES_GARBAGE:
        df_type = df[df['image'].str.contains(type_garbage)]
        df_type.reset_index(drop=True, inplace=True)
        qtde_test = int(len(df_type)*0.3)
        list_of_test = []
        list_of_test = np.random.choice(np.arange(0,len(df_type)),size=qtde_test,replace=False)
        ids_ordenados = sorted(list_of_test)
        df_type_test = df_type.iloc[ids_ordenados]

        # para todas as imagens contidas em garbage, verifica se esta na lista de testes e move para a pasta test
        for garbage in os.listdir('./data/garbage'):
            if garbage in (df_type_test['image'].to_list()):
                if not os.path.exists('./data/test/'):
                    os.makedirs('./data/test/')
                move('./data/garbage/'+garbage, './data/test/'+garbage)

    os.rename('./data/garbage','./data/train')
    
if __name__ == '__main__':
    path_labels = '/Users/pedroflorencio/Documents/PedroFlorencio/EcoSCAN_GITLAB/ecoscan/data/labels/labels.txt'
    train_test_separator(path_labels)
